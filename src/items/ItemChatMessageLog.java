package items;

import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import models.ChatMessage;
import server.Main;

import java.util.ArrayList;

public class ItemChatMessageLog extends ListCell<ChatMessage> {
    //Gerneral
    private Main mainApp;

    //CSS
    private static final String MESSAGE_CONTAINER = "message-container";
    private static final String MESSAGE_CONTENT = "message-content";
    private static final String MESSAGE_META = "message-meta";

    //View
    private GridPane grid = new GridPane();
    private Label content = new Label();
    private Label meta = new Label();

    public ItemChatMessageLog(Main mainApp) {
        this.mainApp = mainApp;
        grid.setHgap(10);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 10, 0, 10));
        grid.getStyleClass().add(MESSAGE_CONTAINER);
        content.getStyleClass().add(MESSAGE_CONTENT);
        content.setWrapText(true);
        meta.getStyleClass().add(MESSAGE_META);

        //add Items
        grid.add(content, 0, 0);
        grid.add(meta, 0, 1);
    }

    /* ------------------------------------- item methods -------------------------------- */
    @Override
    public void updateItem(ChatMessage message, boolean empty) {
        super.updateItem(message, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            addContent(message);
        }
    }

    private void addContent(ChatMessage message) {
        setText(null);

        if (mainApp.getMessagesBundle().containsKey(message.getContent())) {
            content.setText(mainApp.getMessagesBundle().getString(message.getContent()));
        } else {
            content.setText(message.getContent());
        }

        if (!message.getUsername().isEmpty()) {
            meta.setText(message.getMetaAsString());
            if (!grid.getChildren().contains(meta)) { grid.add(meta, 0, 1); }
        } else { if (grid.getChildren().contains(meta)) { grid.getChildren().remove(meta); } }
        
        setGraphic(grid);
    }
}
