package items;

import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import models.ChatMessage;
import skins.Skin;

public class ItemChatMessage extends ListCell<ChatMessage> {
    //General
    private client.Main mainApp;

    //CSS
    private static final String MESSAGE_CONTAINER = "message-container";
    private static final String MESSAGE_ICON = "message-icon";
    private static final String MESSAGE_CONTENT = "message-content";
    private static final String MESSAGE_META = "message-meta";

    //View
    private GridPane grid = new GridPane();
    private TextFlow content = new TextFlow();
    private Label meta = new Label();
    private ImageView icon = new ImageView();

    public ItemChatMessage(client.Main mainApp) {
        this.mainApp = mainApp;
        grid.setHgap(10);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 10, 0, 10));
        grid.getStyleClass().add(MESSAGE_CONTAINER);

        icon.getStyleClass().add(MESSAGE_ICON);
        icon.setPreserveRatio(true);

        content.getStyleClass().add(MESSAGE_CONTENT);

        meta.getStyleClass().add(MESSAGE_META);

        //Add Content
        grid.add(icon, 0, 0, 1, 2);
        grid.add(content, 1, 0);
        grid.add(meta, 1, 1);
    }

    /* ------------------------------------- item methods -------------------------------- */
    @Override
    public void updateItem(ChatMessage message, boolean empty) {
        super.updateItem(message, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            content.getChildren().clear();
            addContent(message);
        }
    }

    private void addContent(ChatMessage message) {
        setText(null);
        if (mainApp.getMessagesBundle().containsKey(message.getContent())) {
            content.getChildren().add(new Text(mainApp.getMessagesBundle().getString(message.getContent())));
        } else {
            if (message.getContent().contains("~")) {
                String messageContent = message.getContent();
                if (messageContent.substring(messageContent.length() - 1).equals("~")) { messageContent += "¶"; }
                String[] part = messageContent.split("~");

                for (int i = 0; i < part.length; i++) {
                    Text newText = new Text(part[i]);
                    if (part.length > 2 && part.length%2 == 1) {
                        if (i%2 == 1) { newText.setStyle("-fx-font-weight: bold"); }
                    }

                    if (!part[i].equals("¶")) { content.getChildren().add(newText); }
                }
            } else {
                content.getChildren().add(new Text(message.getContent()));
            }
        }

        StringBuilder metaContent = new StringBuilder();
        if (!message.getName().isEmpty()) { metaContent.append(message.getName()); }
        if (!message.getName().isEmpty()) { metaContent.append("\t").append(message.getShortTimestamp()); }

        meta.setText(metaContent.toString());

        int xvalue = (message.getIcon()%5)*40;
        int yvalue = (message.getIcon()/5)*40;

        //Icon
        Rectangle2D viewPort = new Rectangle2D(xvalue, yvalue, 40, 40);
        if (message.findItemInMeta("skin") > -1) {
            icon.setImage(Skin.valueOf(message.getInformation("skin")).getIconsAsImage(true));
        } else {
            icon.setImage(mainApp.getSkin().getIconsAsImage(true));
        }
        icon.setPreserveRatio(true);
        icon.setViewport(viewPort);

        setGraphic(grid);
    }
}
