package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChatUser {
    private String username;
    private String password;
    private boolean ban;
    private int id;

    public ChatUser () { this(null,null, -1); }

    public ChatUser (String username, String password, int id) {
        this.username = username;
        this.password = password;
        this.id = id;
    }


    /* ------------------------------------- other -------------------------------- */
    public boolean checkPassword (String input) {
        int result = input.compareTo(password);
        return result == 0;
    }


    /* ------------------------------------- setter -------------------------------- */
    public void setPassword(String password) { this.password = password; }
    public void setBan (boolean status) { if (status) {ban=true;} }
    public void setId (int id) { this.id = id; }


    /* ------------------------------------- getter -------------------------------- */
    public String getUsername() { return username; }
    public int getId() {return id; }
    public boolean isBan() { return ban; }
}
