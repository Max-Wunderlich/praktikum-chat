package models;

import javafx.util.Pair;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatMessage implements Serializable{
    public static final int LOGIN = 0, LOGOUT = 1, MESSAGE = 2,  USERLIST = 3, ROOMLIST = 4, CREATEROOM = 5, UPDATEPASSWORD = 6, USERSTATUS = 7, CHECKNAME = 8, KICK = 9, CHANGEROOM = 10, PRIVATEMESSAGE = 11;
    public static final int ICONSERVER = 25, ICONDEFAULT = 26, ICONINFO = 27, ICONALERT = 28, ICONERROR = 29;

    private int type;
    private int icon;

    private String content;


    private ArrayList<Pair<String, String>> meta;

    public ChatMessage (int type, String content, int icon) {
        this.meta = new ArrayList<>();
        this.type = type;
        this.content = content;
        this.icon = icon;
        this.meta = new ArrayList<>();
    }

    public ChatMessage (int type, String content, String translation, int icon) {
        this(type, content, icon);
        setTranslation(translation);
    }

    public ChatMessage (int type, String content) { this(type, content, ChatMessage.ICONDEFAULT); }

    public ChatMessage (int type) { this(type, "", ChatMessage.ICONDEFAULT); }


    /* ------------------------------------- setter -------------------------------- */
    public void setRoom (String roomname) { meta.add(new Pair<>("room", roomname)); }

    public void addInformation (String key, String value) { meta.add(new Pair<>(key, value)); }

    public void setTranslation (String value) { meta.add(new Pair<>("translation", value)); }


    /* ------------------------------------- getter -------------------------------- */
    public String getInformation (String key) { return getValueFromKey(key); }

    public String getContent() { return content; }

    public String getUsername() { return getValueFromKey("username"); }
    public String getNickname() { return getValueFromKey("nickname"); }

    public String getName() {
        if (findItemInMeta("nickname") > -1){
            return getValueFromKey("nickname");
        } else {
            return getValueFromKey("username");
        }
    }

    public int getType() { return type; }

    public int getIcon () { return icon; }

    public String getTimestamp () { return getValueFromKey("timestamp"); }

    public String getShortTimestamp () { return getValueFromKey("timestamp").substring(0, 5);}

    public String getTranslation () { return getValueFromKey("translation"); }

    private String getValueFromKey (String key) {
        if (!meta.isEmpty()) {
            for (Pair<String, String> aMeta : meta) {
                if (aMeta.getKey().equals(key)) { return aMeta.getValue(); }
            }
        } return "";
    }

    public String getMetaAsString () {
        if (!meta.isEmpty()) {
            StringBuilder output = new StringBuilder();
            output.append("[");
            for (Pair<String, String> aMeta : meta) { output.append(aMeta.getKey()).append(" - ").append(aMeta.getValue()).append("  |  "); }
            output.setLength(output.length() - 5);
            output.append(" ]");
            return output.toString();
        } else { return "";}
    }

    /* ------------------------------------- other -------------------------------- */
    public void generateTimestamp () {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
        meta.add(new Pair<>("timestamp", sdf.format(new Date())));
    }

    public int findItemInMeta (String key) {
        if (!meta.isEmpty()) {
            for (int i = 0; i < meta.size(); i++) { if (meta.get(i).getKey().equals(key)) { return i; } }
        } return -1;
    }
}
