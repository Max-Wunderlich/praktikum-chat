package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "rooms")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChatRoomWrapper {
    @XmlElement(name = "room")
    private List<ChatRoom> rooms = null;

    public List<ChatRoom> getRooms() { return rooms; }

    public void setRooms (List<ChatRoom> rooms) { this.rooms = rooms; }
}
