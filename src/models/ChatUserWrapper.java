package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChatUserWrapper {
    @XmlElement(name = "user")
    private List<ChatUser> users = null;

    public List<ChatUser> getUsers() { return users; }

    public void setUsers(List<ChatUser> users) { this.users = users; }
}
