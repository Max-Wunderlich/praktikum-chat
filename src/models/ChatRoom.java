package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "room")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChatRoom {
    private String roomname;
    private String password;
    private int id;

    public ChatRoom () {this ("","",-1);}

    public ChatRoom (String roomname, int id) { this(roomname, "", id); }

    public ChatRoom (String roomname, String password, int id) {
        this.roomname = roomname.trim();
        this.password = password.trim();
        this.id = id;
    }


    /* ------------------------------------- other -------------------------------- */
    public boolean checkPassword (String input) {
        int result = input.compareTo(password);
        return result == 0;
    }


    /* ------------------------------------- setter -------------------------------- */
    public void setName (String name) { this.roomname = name; }
    public void setPassword (String password) { this.password = password; }
    public void setId (int id) {this.id = id;}

    /* ------------------------------------- getter -------------------------------- */
    public String getRoomname() { return roomname; }
    public int getID () {return id;}
    public boolean isPrivate(){ return !password.isEmpty(); }
}
