package skins;

import javafx.scene.image.Image;

public enum Skin {
    DEFAULT("Default", "default", new String[] {"Garry","Christine", "Thomas", "Stephanie", "Viola", "Alexandra", "David", "Benjamin", "Violett", "Nick", "Jackson", "Penny", "Joselyne", "Alexandra", "Robert"}),
    ZOO("Animals", "zoo", new String[]{"Hedgehog","Seagull","Crocodile","Kiwi","Starfish","Snake","Koala bear","Squirrel","Worm","Pig","Whale","Elephant","Penguin",
    "Frog","Puffer","Sheep","Red panda","Crab","Walrus","Philippine tarsier","Fox","Jellyfish","Skunk","Panda","Badger"});

    private final String title;
    private final String internalName;
    private final String[] avatarNames;

    Skin(String title, String internalName, String[] avatarNames) {
        this.title = title;
        this.internalName = internalName;
        this.avatarNames = avatarNames;
    }

    public Image getApplicationIcon () {
        return new Image(getClass().getResourceAsStream("../skins/" + internalName + "_icon.png"));
    }

    public String getIconsAsPath(boolean small) {
        return small ? "../skins/" + internalName + "_small.png" : "../skins/" + internalName + ".png";
    }

    public Image getIconsAsImage ( boolean small) { return new Image(getClass().getResourceAsStream(getIconsAsPath(small))); }

    public String getCSSPath () {
        return "../skins/" + internalName + ".css";
    }

    public String[] getAvatarNames() {
        return avatarNames;
    }

    public String getTitle() {
        return title;
    }
    public String getInternalName() {
        return internalName;
    }
}