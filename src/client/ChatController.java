package client;

import items.ItemChatMessage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.media.AudioClip;
import javafx.util.Callback;
import javafx.util.Pair;
import models.ChatMessage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import skins.Skin;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class ChatController {
    //General
    private Main mainApp;

    // ChatFrame
    @FXML
    TextField tfMessage;
    @FXML
    ListView<ChatMessage> lvChat;
    private ObservableList<ChatMessage> listChat = FXCollections.observableArrayList();

    // User/s
    @FXML
    ListView<String> lvUsers;
    private ObservableList<String> listUsernames = FXCollections.observableArrayList();
    @FXML
    Label labelProfileInfoUsername, labelProfileInfoNickname, labelProfileInfoRoom;
    @FXML
    ImageView ivProfilePicture;

    //Rooms
    @FXML
    ListView<String> lvRooms;
    private ObservableList<String> listRooms = FXCollections.observableArrayList();

    //Menu
    @FXML
    Menu menuView;
    @FXML
    Menu menuFile;
    private List<Pair<String,String>> languageList = new ArrayList<Pair<String,String>>();
    private MenuItem menuItemSound;
    private MenuItem menuItemAlertUserStatus;


    //Sound
    private static final AudioClip ALERT_AUDIOCLIP = new AudioClip(ChatController.class.getResource("../resources/alert.wav").toString());

    /* ------------------------------------- procedure methods -------------------------------- */
    @FXML
    private void initialize() {
        lvChat.setItems(listChat);
        lvUsers.setItems(listUsernames);
        lvRooms.setItems(listRooms);

        lvChat.setCellFactory(new Callback<ListView<ChatMessage>, ListCell<ChatMessage>>() {
            @Override
            public ListCell<ChatMessage> call(ListView<ChatMessage> list) {
                return new ItemChatMessage(mainApp);
            }
        });

        initializeMenu();
    }

    private void initializeMenu () {
        //MenuFile
        menuItemSound = new MenuItem();
        menuFile.getItems().add(0, menuItemSound);
        menuItemSound.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) { changeSoundVolume(); }
        });

        menuItemAlertUserStatus = new MenuItem();
        menuFile.getItems().add(1, menuItemAlertUserStatus);
        menuItemAlertUserStatus.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) { changeAlertUserStatus(); }
        });

        //MenuView
        ToggleGroup skinsToggleGroup = new ToggleGroup();

        List<Skin> valueList = new ArrayList<Skin>(EnumSet.allOf(Skin.class));

        for (Skin current : valueList) {
            RadioMenuItem menuItem = new RadioMenuItem();
            menuItem.setToggleGroup(skinsToggleGroup);
            menuItem.setText(current.getTitle());
            menuItem.setSelected(false);
            menuItem.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent event) {
                    mainApp.changeSkin(current);
                }
            });

            menuView.getItems().add(menuItem);
        }

        menuView.getItems().add(new SeparatorMenuItem());

        ToggleGroup languageToggleGroup = new ToggleGroup();

        languageList.add(new Pair<>("en", "English"));
        languageList.add(new Pair<>("de", "Deutsch"));

        for (Pair<String,String> current : languageList) {
            RadioMenuItem menuItem = new RadioMenuItem();
            menuItem.setToggleGroup(languageToggleGroup);
            menuItem.setText(current.getValue());
            menuItem.setSelected(false);
            menuItem.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent event) {
                    mainApp.changeLanguage(current.getKey());
                }
            });

            menuView.getItems().add(menuItem);
        }
    }

    void updateInterfaceAfterLogin () {
        String targetLanguage = "";
        for (Pair<String,String> languageValue: languageList) {
            if (languageValue.getKey().equals(mainApp.getConfig().getLanguage())) {
                targetLanguage = languageValue.getValue();
            }
        }

        //Check right SkinItem
        ObservableList<MenuItem> viewItems = menuView.getItems();
        for (MenuItem current : viewItems) {
            if (current instanceof RadioMenuItem) {
                RadioMenuItem currentRadio = (RadioMenuItem) current;
                if (currentRadio.getText().equals(mainApp.getSkin().getTitle())) {
                    currentRadio.setSelected(true);
                } else if (currentRadio.getText().equals(targetLanguage)) {
                    currentRadio.setSelected(true);
                }
            }
        }

        menuItemSound.setText(mainApp.getMessagesBundle().getString("AlertSound"));
        if (!mainApp.getConfig().isMute()) {
            menuItemSound.getStyleClass().add("icon-volume");
        } else {
            menuItemSound.getStyleClass().add("icon-volume-mute");
        }

        menuItemAlertUserStatus.setText(mainApp.getMessagesBundle().getString("AlertUserStatus"));
        if (mainApp.getConfig().isAlertUserStatus()) {
            menuItemAlertUserStatus.getStyleClass().add("icon-alert-user-status");
        } else {
            menuItemAlertUserStatus.getStyleClass().add("icon-alert-user-status-disable");
        }
    }

    @FXML
    void logout () {
        mainApp.getClient().sendLogoutData(false);
        clearChat();
        Platform.runLater(() -> {  mainApp.showLoginScreen();});
    }

    @FXML
    void closeClient() {
        mainApp.getClient().sendLogoutData(true);
        mainApp.closeApplication();
    }


    /* ------------------------------------- chat methods -------------------------------- */
    @FXML
    private void messageEnter (KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String message = tfMessage.getText().trim();
            message = message.replace("  ", " ");
            if (!message.isEmpty()) { mainApp.client.sendMessage(message); }
            tfMessage.clear();
        }
    }

    public void displayMessage (ChatMessage message) {
        int index = lvChat.getItems().size();

        Platform.runLater(() -> {
            listChat.add(message);
            lvChat.scrollTo(index);
        });

        if (!message.getUsername().equals(mainApp.getClient().getUsername())) {
            if (!ALERT_AUDIOCLIP.isPlaying() && !mainApp.getConfig().isMute()){
                ALERT_AUDIOCLIP.play();
            }
        }
    }

    @FXML
    void clearChat () { Platform.runLater(() -> { listChat.clear(); }); }


    /* ------------------------------------- user actions -------------------------------- */
    //User
    @FXML
    private void updateNickname () {
        String input = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogUpdateNicknameTitle"),mainApp.getMessagesBundle().getString("DialogUpdateNicknameContent"), mainApp.getClient().getNickname());
        input.replace(" ", "");
        if (input.length() > 3) {
            mainApp.getClient().sendMessage(new ChatMessage(ChatMessage.CHECKNAME, input));
        } else {
            displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("ErrorMoreCharacters"), ChatMessage.ICONSERVER));
        }
    }

    @FXML
    private void updatePassword () {
        String input = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogUpdatePasswordTitle"),mainApp.getMessagesBundle().getString("DialogUpdatePasswordContent"),"");
        input.replace(" ", "");
        if (input.length() > 3) {
            mainApp.getClient().sendMessage(ChatMessage.UPDATEPASSWORD, input);
        } else {
            displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("ErrorMoreCharacters"), ChatMessage.ICONALERT));//???
        }
    }

    @FXML
    public void updateProfileImage () {
        int tempicon = mainApp.showProfileImageDialog(mainApp.getClient().getIcon());
        mainApp.getClient().setIcon(tempicon);
        updateProfileImageView(tempicon);
    }

    //Room
    @FXML
    void createRoom() {
        String name = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogCreateRoomTitle"), mainApp.getMessagesBundle().getString("DialogCreateRoomContentName"), "");
        if (!name.isEmpty() && !name.contains("*")) {
            mainApp.getClient().sendMessage(ChatMessage.CREATEROOM, name);
        } else {
            displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorCreateRoom", ChatMessage.ICONERROR));
        }
    }

    @FXML
    void enterRoom () {
        int input = lvRooms.getSelectionModel().getSelectedIndex();
        if (input > -1) {
            String name = listRooms.get(input);
            if (name.substring(0, 1).equals("*")) {
                String inputpassword = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogRoomPasswordTitle"),mainApp.getMessagesBundle().getString("DialogRoomPasswordContent"),"").trim();
                if (!inputpassword.isEmpty()) {
                    ChatMessage newMessage = new ChatMessage(ChatMessage.CHANGEROOM, listRooms.get(input).substring(1));
                    newMessage.addInformation("password", inputpassword);
                    mainApp.getClient().sendMessage(newMessage);
                } else { displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("ErrorNeedRoomPassword"), ChatMessage.ICONERROR)); }
            } else { mainApp.getClient().sendMessage(ChatMessage.CHANGEROOM, listRooms.get(input)); }
        }
    }

    //Others
    private void changeSoundVolume () {
        menuFile.getItems().remove(menuItemSound);
        if (mainApp.getConfig().isMute()) {
            menuItemSound.getStyleClass().remove("icon-volume-mute");
            menuItemSound.getStyleClass().add("icon-volume");
        } else {
            menuItemSound.getStyleClass().remove("icon-volume");
            menuItemSound.getStyleClass().add("icon-volume-mute");
        }

        mainApp.getConfig().setMute(!mainApp.getConfig().isMute());
        System.out.println(mainApp.getConfig().isMute());
        menuFile.getItems().add(0, menuItemSound);
    }

    private void changeAlertUserStatus () {
        menuFile.getItems().remove(menuItemAlertUserStatus);
        if (mainApp.getConfig().isAlertUserStatus()) {
            menuItemAlertUserStatus.getStyleClass().remove("icon-alert-user-status");
            menuItemAlertUserStatus.getStyleClass().add("icon-alert-user-status-disable");
        } else {
            menuItemAlertUserStatus.getStyleClass().remove("icon-alert-user-status-disable");
            menuItemAlertUserStatus.getStyleClass().add("icon-alert-user-status");
        }

        mainApp.getConfig().setAlertUserStatus(!mainApp.getConfig().isAlertUserStatus());
        System.out.println(mainApp.getConfig().isAlertUserStatus());
        menuFile.getItems().add(1, menuItemAlertUserStatus);
    }

    @FXML
    void openHelpPage () throws URISyntaxException {
        try { openWebpage( new URL("http://uni.maxwunderlich.de/informatik/chat/help.php").toURI()); }
        catch (MalformedURLException e) { e.printStackTrace(); }
    }

    @FXML
    void openAboutPage () throws URISyntaxException {
        try { openWebpage( new URL("http://uni.maxwunderlich.de/informatik/chat/about.php").toURI()); }
        catch (MalformedURLException e) { e.printStackTrace(); }
    }


    /* ------------------------------------- update methods -------------------------------- */
    public void updateUserlist (String list) {
        Platform.runLater(() -> {
                lvUsers.setItems(null);
                listUsernames.clear();
                String[] parts = list.split("‰");
                listUsernames.addAll(Arrays.asList(parts));
                lvUsers.setItems(listUsernames);
        });
    }

    public void updateRoomlist (String list) {
        Platform.runLater(() -> {
                lvRooms.setItems(null);
                listRooms.clear();
                String[] parts = list.split("‰");

                listRooms.addAll(Arrays.asList(parts));
                lvRooms.setItems(listRooms);
        });
    }

    public void updateProfileImageView (int icon) {
        int xvalue = (icon%5)*125;
        int yvalue = (icon/5)*125;

        Rectangle2D viewPort = new Rectangle2D(xvalue, yvalue, 125, 125);
        ivProfilePicture.setImage(mainApp.getSkin().getIconsAsImage(false));
        ivProfilePicture.setViewport(viewPort);
    }

    void printUserInformation() {
        Platform.runLater(() -> {
                labelProfileInfoUsername.setText(mainApp.getClient().getUsername());
                labelProfileInfoNickname.setText(mainApp.getClient().getNickname());
                labelProfileInfoRoom.setText(mainApp.getClient().getCurrentRoom());
        });
    }


    /* ------------------------------------- setter -------------------------------- */
    public void setMainApp (Main mainApp) { this.mainApp = mainApp; }


    /* ------------------------------------- others -------------------------------- */
    private static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try { desktop.browse(uri); }
            catch (Exception e) { e.printStackTrace(); }
        }
    }


    //////////////Neu
    @FXML
    void createPrivateChat () {
        int index = lvUsers.getSelectionModel().getSelectedIndex();
        if (index > -1 && !listUsernames.get(index).equals(mainApp.getClient().getUsername())) { mainApp.generatePrivateChat(listUsernames.get(index));
        } else {
            displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorPrivateConnection", ChatMessage.ICONALERT));
        }
    }
}
