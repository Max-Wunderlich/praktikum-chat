package client;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class LoginController {
    //General
    Main mainApp;

    //View
    @FXML
    TextField tfUsername;
    @FXML
    PasswordField tfPassword;
    @FXML
    Button btnLoginSubmit;
    @FXML
    Label labelErrorMessage;

    /* ------------------------------------- methods -------------------------------- */
    @FXML
    private void loginEnter (KeyEvent Login) {
        if (Login.getCode() == KeyCode.ENTER) { submitInput(); }
    }

    @FXML
    private void submitInput () {
        labelErrorMessage.setVisible(false);
        tfUsername.setDisable(true);
        tfPassword.setDisable(true);
        btnLoginSubmit.setDisable(true);

        String username = tfUsername.getText();
        String password = tfPassword.getText();
        username = username.replace(" ", "");
        password = password.replace(" ", "");

        if (password.length() > 3 && username.length() > 3) {
            mainApp.getClient().sendLoginData(username, password);
        } else {
            displayError(mainApp.getMessagesBundle().getString("ErrorMoreCharacters"));
        }
    }

    void showResult (String result) {
        switch (result) {
            case "register": {
                mainApp.closeLoginScreen();
                break;
            }

            case "login" : {
                mainApp.closeLoginScreen();
                break;
            }

            case "fail-input" : {
                displayError(mainApp.getMessagesBundle().getString("ErrorInput"));
                break;
            }

            case "fail-otherconnection" : {
                displayError(mainApp.getMessagesBundle().getString("ErrorOtherConnection"));
                break;
            }

            case "fail-ban" : {
                displayError(mainApp.getMessagesBundle().getString("ErrorBan"));
                break;
            }

            case "fail-forbidden" : {
                displayError(mainApp.getMessagesBundle().getString("ErrorIllegalName"));
                break;
            }

            default: {
                displayError(result);
            }
        }
    }

    private void displayError(String message){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelErrorMessage.setText(message);
                labelErrorMessage.setVisible(true);

                tfUsername.setDisable(false);
                tfPassword.setDisable(false);
                btnLoginSubmit.setDisable(false);
            }
        });
    }

    /* ------------------------------------- setter -------------------------------- */
    public void setMainApp (Main mainApp) { this.mainApp = mainApp; }

}
