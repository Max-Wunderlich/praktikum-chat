package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatPrivateServer {
    private int port;
    private ServerSocket serverSocket;
    private ConnectionThread connectionA;
    private ConnectionThread connectionB;
    Socket client;

    ChatPrivateServer(int port) {
        this.port = port;
    }


    void runServer () {
        try {
            serverSocket = new ServerSocket(port);
            LoginThread thread = new LoginThread(serverSocket);
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void stopService() {
        try {
            serverSocket.close();
            System.out.println("--- Socket closed ---");
        } catch (IOException e) { e.printStackTrace(); }
    }

    class ConnectionThread extends Thread {
        Socket socket;
        ObjectInputStream inputStream;
        ObjectOutputStream outputStream;


        ConnectionThread(Socket socket) { this.socket = socket; }


        @Override
        public void run() {
            try {
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                inputStream = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            Boolean online = true;

            while (online) {
                try {
                    String message = (String) inputStream.readObject();

                    connectionA.sendMessageToClient(message);
                    connectionB.sendMessageToClient(message);

                    if (message.contains("⅞")) {
                        online = false;
                        stopService();
                    }
                } catch (IOException | ClassNotFoundException e) {
                    online = false;
                }
            }
        }

        /* ------------------------------------- send method -------------------------------- */
        public void sendMessageToClient(String message) {
            try { outputStream.writeObject(message); }
            catch (IOException e) { e.printStackTrace(); }
        }
    }

    class LoginThread extends Thread {
        ServerSocket serverSocket;

        LoginThread(ServerSocket serverSocket) {
            this.serverSocket = serverSocket;
        }


        @Override
        public void run() {
            try {
                client = serverSocket.accept();
                connectionA =  new ConnectionThread(client);
                connectionA.start();
                client = serverSocket.accept();
                connectionB =  new ConnectionThread(client);
                connectionB.start();
            } catch (IOException e) {System.out.println(e.getMessage()); }
        }
    }
}
