package client;

import skins.Skin;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Config {
    //Connection
    private String ip;
    private int port;
    private String language;

    //View
    private Skin skin;

    //Sound
    private boolean mute;

    //Other
    private boolean alertuserstatus;

    /* ------------------------------------- setter -------------------------------- */
    @XmlElement
    public void setIp(String ip) { this.ip = ip; }
    @XmlElement
    public void setPort(int port) { this.port = port; }
    @XmlElement
    public void setSkin(Skin skin) { this.skin = skin; }
    @XmlElement
    public void setLanguage(String language) { this.language = language; }
    @XmlElement
    public void setMute(boolean mute) { this.mute = mute; }
    @XmlElement
    public void setAlertUserStatus(boolean alertuserstatus) { this.alertuserstatus = alertuserstatus; }

    /* ------------------------------------- getter -------------------------------- */
    public String getIp() { return ip; }
    public int getPort() { return port; }
    public Skin getSkin() { return skin; }
    public String getLanguage() { return language; }
    public boolean isMute() { return mute; }
    public boolean isAlertUserStatus() { return alertuserstatus; }
}