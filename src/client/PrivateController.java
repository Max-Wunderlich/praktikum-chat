package client;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.ResourceBundle;


public class PrivateController {

    ResourceBundle messagesBundle;

    ChatPrivateClient client;
    @FXML
    ListView<String> lvChat;
    @FXML
    TextField tfMessage;
    private ObservableList<String> listChat = FXCollections.observableArrayList();


    @FXML
    private void initialize() {
        lvChat.setItems(listChat);
    }

    void disableMessageField () { tfMessage.setDisable(true); }

    void stop () { if (!client.isOffline()) { client.sendMessage("⅞"); } }

    @FXML
    private void messageEnter (KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String message = tfMessage.getText().trim();
            message = message.replace("  ", " ");
            if (!message.isEmpty()) { client.sendMessage(message); }
            tfMessage.clear();
        }
    }

    public void displayMessage (String message) {
        if (messagesBundle.containsKey(message)) { message = messagesBundle.getString(message); }
        int index = lvChat.getItems().size();

        String finalMessage = message;
        Platform.runLater(() -> {
            listChat.add(finalMessage);
            lvChat.scrollTo(index);
        });
    }

    public void setClient (ChatPrivateClient client) { this.client = client; }
    public void setMessagesBundle(ResourceBundle messagesBundle) { this.messagesBundle = messagesBundle; }
}
