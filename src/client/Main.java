package client;

import helper.UTF8Control;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.ChatMessage;
import skins.Skin;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

public class Main extends Application {
    ChatClient client;
    private Stage chatStage;
    private Stage loginStage;
    private ChatController chatController;
    private LoginController loginController;
    private Config config;
    private ResourceBundle messagesBundle;


    /* ------------------------------------- procedure methods -------------------------------- */
    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage chatStage) throws Exception{
        this.chatStage = chatStage;
        importConfig();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("chat.fxml"), messagesBundle);

        BorderPane root = loader.load();
        chatStage.setTitle(messagesBundle.getString("ClientName"));
        chatStage.setScene(new Scene(root, 800, 600));
        chatStage.getIcons().add(config.getSkin().getApplicationIcon());
        chatStage.setOnCloseRequest(event -> { chatController.closeClient(); });

        chatStage.getScene().getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        chatStage.getScene().getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());

        chatController = loader.getController();
        chatController.setMainApp(this);

        client = new ChatClient(config.getIp(), config.getPort(), this);
        client.initializeService();

        chatStage.show();
        showLoginScreen();
    }

    private void importConfig () {
        File file = new File("config_client.xml");

        if(file.exists() && !file.isDirectory()) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                config = (Config) jaxbUnmarshaller.unmarshal(file);
            } catch (JAXBException e) { e.printStackTrace(); }
        } else {
            config = new Config();
            config.setSkin(Skin.DEFAULT);
            config.setIp(showInputTextDialog("Server", "IP", "localhost"));
            config.setPort(1789);
            config.setLanguage("en");
            config.setMute(false);
            config.setAlertUserStatus(true);
        }

        loadLanguage();
    }

    private void loadLanguage () {
        String baseName = "resources.Messages";
        Locale.setDefault( new Locale(config.getLanguage()) );
        try { messagesBundle = ResourceBundle.getBundle(baseName, new UTF8Control()); }
        catch ( MissingResourceException e ) { System.err.println( e ); }
    }

    private void exportConfig () {
        try {
            File file = new File("config_client.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(config, file);
        } catch (JAXBException e) { e.printStackTrace(); }

    }

    void closeApplication () {
        exportConfig();
        Platform.exit();
    }

    @Override
    public void stop() throws Exception { super.stop(); }


    /* ------------------------------------- Login -------------------------------- */
    public void showLoginScreen () {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"),messagesBundle);
            VBox page = loader.load();
            loginStage = new Stage();
            loginStage.setTitle(messagesBundle.getString("Login"));
            loginStage.initModality(Modality.WINDOW_MODAL);
            loginStage.initOwner(chatStage);
            Scene scene = new Scene(page);
            loginStage.setScene(scene);
            loginStage.setResizable(false);
            loginStage.getIcons().add(config.getSkin().getApplicationIcon());
            loginController = loader.getController();
            loginController.setMainApp(this);

            loginStage.getScene().getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
            loginStage.getScene().getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());

            loginStage.setOnCloseRequest(event -> { chatController.closeClient(); });

            loginStage.show();
        } catch (IOException  e) { e.printStackTrace(); }
    }

    void closeLoginScreen() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                loginStage.close();
                chatStage.setTitle(messagesBundle.getString("ClientName") + " - " + client.getUsername());
                getChatController().printUserInformation();
            }});
    }


    /* ------------------------------------- dialogs -------------------------------- */
    String showInputTextDialog(String title, String content, String defaultValue) {
        TextInputDialog dialog = new TextInputDialog(defaultValue);
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.setContentText(content);
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        dialogPane.getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());
        dialogPane.getStyleClass().add("gradientbackground");
        dialog.initStyle(StageStyle.UTILITY);

        Optional<String> result = dialog.showAndWait();

        return result.orElse(defaultValue);
    }

    int showProfileImageDialog (int defaultValue) {
        List<String> choices = Arrays.asList(config.getSkin().getAvatarNames());

        ChoiceDialog<String> dialog = new ChoiceDialog<>(choices.get(defaultValue), choices);
        dialog.setTitle(messagesBundle.getString("DialogChangeProfilePictureTitle"));
        dialog.setHeaderText(null);
        dialog.setContentText(messagesBundle.getString("DialogChangeProfilePictureContent"));
        dialog.initStyle(StageStyle.UTILITY);
        loadStyle(dialog.getDialogPane());

        Optional<String> result = dialog.showAndWait();
        return result.map(choices::indexOf).orElse(defaultValue);
    }


    /* ------------------------------------- setter -------------------------------- */
    void changeSkin (Skin skin) {
        config.setSkin(skin);
        chatStage.getScene().getStylesheets().clear();
        chatStage.getScene().getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        chatStage.getScene().getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());

        client.setIcon(0);
        chatController.updateProfileImageView(client.getIcon());
    }

    void changeLanguage (String lang) {
        config.setLanguage(lang);
        chatController.displayMessage(new ChatMessage (ChatMessage.MESSAGE, messagesBundle.getString("AlertRestart") ,ChatMessage.ICONALERT));
    }


    /* ------------------------------------- getter -------------------------------- */
    ChatClient getClient() {return client;}
    ChatController getChatController() {return chatController;}
    LoginController getLoginController() {return loginController;}
    public Config getConfig() { return config; }
    public Skin getSkin () {return config.getSkin();}
    public ResourceBundle getMessagesBundle() { return messagesBundle; }


    /* ------------------------------------- other -------------------------------- */
    private void loadStyle (DialogPane dialogPane) {
        dialogPane.getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        dialogPane.getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());
        dialogPane.getStyleClass().add("gradientbackground");
    }

    void restartClientAfterNewIP(){
        config.setIp(showInputTextDialog(messagesBundle.getString("DialogNewIPTitle"), messagesBundle.getString("DialogNewIPContent"), "localhost"));
        client = new ChatClient(config.getIp(), 1789, this);
        client.initializeService();
    }



    //////////Neu
    void startPrivateChat (String input, String port, boolean first) {
        ChatPrivateServer privateServer = null;
        int portInt = Integer.valueOf(port);

        if (first) {
            privateServer = new ChatPrivateServer(portInt);
            privateServer.runServer();
            //Send Message to the other person
            //client.sendMessage(new ChatMessage(ChatMessage.PRIVATEMESSAGE, input));
            input = "localhost";
        }

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("private.fxml"), messagesBundle);
            BorderPane page = loader.load();
            Stage privateStage = new Stage();
            privateStage.setTitle(messagesBundle.getString("Private"));
            privateStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page);
            privateStage.setScene(scene);
            privateStage.setResizable(false);
            privateStage.getIcons().add(config.getSkin().getApplicationIcon());
            PrivateController privateController = loader.getController();
            privateController.setMessagesBundle(messagesBundle);

            System.out.println(input);
            ChatPrivateClient chatPrivateClient = new ChatPrivateClient(input, portInt, privateController, client.getUsername());
            chatPrivateClient.initializeService();
            privateController.setClient(chatPrivateClient);

            privateStage.getScene().getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
            privateStage.getScene().getStylesheets().add(getClass().getResource(config.getSkin().getCSSPath()).toExternalForm());

            privateStage.setOnCloseRequest(event -> {
                privateController.stop();
            });

            privateStage.show();
        } catch (IOException  e) { e.printStackTrace(); }

    }

    void generatePrivateChat (String input) {
        client.sendMessage(new ChatMessage(ChatMessage.PRIVATEMESSAGE, input));
    }
}
