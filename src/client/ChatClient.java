package client;

import javafx.application.Platform;
import models.ChatMessage;
import server.ChatServer;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient {
    //General
    private Main mainApp;

    //Connection
    private Socket socket;
    private ObjectOutputStream outputStream;
    private String host;
    private int port;

    //Userdata
    private String username;
    private String nickname;
    private int icon = 0;
    private String currentRoom;

    ChatClient (String host, int port, Main mainApp) {
        this.port = port;
        this.host = host;
        this.mainApp = mainApp;
        this.currentRoom = "default";
    }


    /* ------------------------------------- procedure methods -------------------------------- */
    public void initializeService () {
        try {
            socket = new Socket(host, port);

            outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            ReceiveMessageThread receiveMessageThread = new ReceiveMessageThread(inputStream);
            receiveMessageThread.start();

        } catch(UnknownHostException e) {
            System.out.println("Can´t find host.");
            mainApp.restartClientAfterNewIP();
        } catch (IOException e) {
            System.out.println("Error connecting to host.");
            mainApp.restartClientAfterNewIP();
        }
    }

    private void stopService () {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--- Service closed ---");
    }


    /* ------------------------------------- send data methods -------------------------------- */
    public void sendMessage (String message) { sendMessage(new ChatMessage(ChatMessage.MESSAGE, message, icon)); }

    public void sendMessage (int type, String message) { sendMessage(new ChatMessage(type, message)); }

    public void sendMessage (ChatMessage message) {
        message.addInformation("skin", String.valueOf(mainApp.getConfig().getSkin()));
        message.addInformation("username", getUsername());
        if (isNicknameSet()) {message.addInformation("nickname", getNickname());}
        try {
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendLoginData (String inputUsername, String inputPassword) {
        try {
            ChatMessage tempChatMessage = new ChatMessage(ChatMessage.LOGIN, inputUsername);
            tempChatMessage.addInformation("password", inputPassword);
            outputStream.writeObject(tempChatMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendLogoutData (boolean closeConnection) {
        ChatMessage logoutmessage = new ChatMessage(ChatMessage.LOGOUT);

        if (closeConnection) { logoutmessage.addInformation("close","");}

        try {
            outputStream.writeObject(logoutmessage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (closeConnection) { stopService(); }
    }


    /* ------------------------------------- setter -------------------------------- */
    public void setIcon(int icon) { this.icon = icon; }


    /* ------------------------------------- getter -------------------------------- */
    public String getUsername () {return username;}
    public String getNickname () {return nickname;}
    private boolean isNicknameSet () { return !nickname.isEmpty(); }
    public String getCurrentRoom() { return currentRoom; }
    public int getIcon() { return icon; }


    /* ------------------------------------------------------------------------------ */
    /* ------------------------------------- THREADS -------------------------------- */
    /* ------------------------------------------------------------------------------ */
    class ReceiveMessageThread extends Thread {
        ObjectInputStream inputStream;
        Boolean online;

        ReceiveMessageThread (ObjectInputStream inputStream) {
            this.inputStream = inputStream;
            this.online = true;
        }

        @Override
        public void run() {
            while (online) {
                try {
                    ChatMessage message = (ChatMessage) inputStream.readObject();

                    switch (message.getType()) {
                        case ChatMessage.LOGIN: {
                            String result = message.getContent();

                            if (result.equals("register") || result.equals("login")) {
                                //Login Successful
                                username = message.getInformation("username");
                                nickname = "";

                                mainApp.getChatController().updateProfileImageView(0);
                                mainApp.getChatController().updateInterfaceAfterLogin();
                            }

                            mainApp.getLoginController().showResult(result);
                            break;
                        }

                        case ChatMessage.USERLIST: {
                            mainApp.getChatController().updateUserlist(message.getContent());
                            break;
                        }

                        case ChatMessage.ROOMLIST: {
                            mainApp.getChatController().updateRoomlist(message.getContent());
                            break;
                        }

                        case ChatMessage.KICK: {
                            mainApp.getChatController().logout();
                            break;
                        }

                        case ChatMessage.CHANGEROOM: {
                            currentRoom = message.getContent();
                            mainApp.getChatController().clearChat();
                            mainApp.getChatController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("GreetingNewRoom")+ ": " + message.getContent(), ChatMessage.ICONSERVER));//?????
                            mainApp.getChatController().printUserInformation();
                            break;
                        }

                        case ChatMessage.CREATEROOM: {
                            sendMessage(ChatMessage.CHANGEROOM, message.getContent());
                            break;
                        }

                        case ChatMessage.CHECKNAME: {
                            if (!message.getContent().isEmpty()) {
                                if (message.getContent().equals(username)) {
                                    //Empty Nickname
                                    nickname = "";
                                    mainApp.getChatController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("SuccessNoNickname")));
                                } else {
                                    nickname = message.getContent();
                                    mainApp.getChatController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("SuccessNewNickname")));
                                }

                                mainApp.getChatController().printUserInformation();
                            } else {
                                mainApp.getChatController().displayMessage(new ChatMessage(ChatMessage.MESSAGE,mainApp.getMessagesBundle().getString("ErrorIllegalName"), ChatMessage.ICONERROR));
                            }
                            break;
                        }

                        case ChatMessage.PRIVATEMESSAGE: {
                            if (message.getInformation("ip").isEmpty()) {
                                Platform.runLater(() -> {mainApp.startPrivateChat("", message.getContent(), true);});
                            } else {
                                Platform.runLater(() -> { mainApp.startPrivateChat(message.getInformation("ip"), message.getContent(), false);});
                            }

                            break;
                        }

                        case ChatMessage.USERSTATUS: {
                            if (!mainApp.getConfig().isAlertUserStatus()) {break;}
                        }

                        case ChatMessage.MESSAGE: {
                            if (!message.getTranslation().isEmpty()) {
                                StringBuilder output = new StringBuilder();
                                String[] parts = message.getTranslation().split("½");
                                for (String part: parts) {
                                    if (mainApp.getMessagesBundle().containsKey(part)) {
                                        output.append(mainApp.getMessagesBundle().getString(part));
                                    } else { output.append(part); }
                                    output.append(" ");
                                } mainApp.getChatController().displayMessage(new ChatMessage(message.getType(), output.toString(), message.getIcon()));
                            } else { mainApp.getChatController().displayMessage(message); }

                            if (!message.getInformation("room").isEmpty() && !message.getInformation("room").equals(currentRoom)) {
                                currentRoom = message.getInformation("room");
                                mainApp.getChatController().printUserInformation();
                            }
                            break;
                        }

                        default: { System.out.println(message.getContent()); }
                    }
                } catch (IOException e) {
                    System.out.println("--- Connection closed ---");
                    online = false;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}