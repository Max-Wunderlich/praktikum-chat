package client;

import models.ChatMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatPrivateClient {
    private Socket socket;
    private ObjectOutputStream outputStream;
    private String host;
    private int port;
    private PrivateController controller;
    private String username;
    private boolean offline;

    ChatPrivateClient (String host, int port, PrivateController controller, String username) {
        this.port = port;
        this.host = host;
        this.controller = controller;
        this.username=username;
        offline = false;
    }

    public void initializeService () {
        try {
            //socket = new Socket("10.232.61.6", port);
            socket = new Socket(host, port);

            outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            ReceiveMessageThread receiveMessageThread = new ReceiveMessageThread(inputStream);
            receiveMessageThread.start();

        } catch(UnknownHostException e) {
            System.out.println("Can´t find host.");
        } catch (IOException e) {
            System.out.println("Error connecting to host.");
        }
    }

    private void stopService() {
        offline = true;
        try { socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--- Service closed ---");
    }

    public void sendMessage (String message) {
        try {
            outputStream.writeObject(username + ": \t" + message);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isOffline() { return offline; }

    class ReceiveMessageThread extends Thread {
        ObjectInputStream inputStream;
        Boolean online;

        ReceiveMessageThread (ObjectInputStream inputStream) {
            this.inputStream = inputStream;
            this.online = true;
        }

        @Override
        public void run() {
            while (online) {
                try {
                    String message = (String) inputStream.readObject();

                    if (message.contains("⅞")) {
                        controller.disableMessageField();
                        online=false;
                        message="AlertPrivateServerClosed";
                    }

                    controller.displayMessage(message);
                } catch (IOException e) {
                    System.out.println("--- Connection closed ---");
                    online = false;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

            stopService();
        }
    }

    public String getUsername() { return username; }
}
