package server;

import helper.UTF8Control;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.StageStyle;
import models.*;
import skins.Skin;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

public class Main extends Application {
    private ChatServer server;
    private Stage dashboardStage;
    private DashboardController dashboardController;
    private server.Config config;
    private Skin skin;
    private ResourceBundle messagesBundle;
    private BufferedWriter logWriter;
    private boolean error = false;


    /* ------------------------------------- procedure methods -------------------------------- */
    //start
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage dashboardStage) throws Exception{
        skin = Skin.DEFAULT;

        FileWriter fw = new FileWriter("log_" + String.valueOf(System.currentTimeMillis()) +".txt", true);
        logWriter = new BufferedWriter(fw);
        addToLog("SERVERLOG", false);

        importConfig();

        this.dashboardStage = dashboardStage;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("dashboard.fxml"),messagesBundle);

        BorderPane root = loader.load();
        dashboardStage.setTitle(messagesBundle.getString("ServerName"));
        dashboardStage.setScene(new Scene(root, 800, 600));
        dashboardStage.getIcons().add(skin.getApplicationIcon());
        dashboardStage.setOnCloseRequest(event -> { dashboardController.shutdown(); });

        dashboardController = loader.getController();
        dashboardController.setMainApp(this);
        dashboardStage.getScene().getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        dashboardStage.getScene().getStylesheets().add(getClass().getResource(skin.getCSSPath()).toExternalForm());

        server = new ChatServer(config.getPort(), this);
        setupServer();

        if (server.initializeService()) {
            dashboardStage.show();
        } else { showErrorDialog("ErrorStartServerTitle", "ErrorStartServerContent"); }
    }

    private void setupServer() {
        if (showYesNoDialog(messagesBundle.getString("DialogImportDataTitle"), messagesBundle.getString("DialogImportDataUsers"))) {
            File sourcefile = showFileChooser(messagesBundle.getString("FileChooserTitle"), true);
            if (sourcefile != null) { importUsers(sourcefile); }

            if (showYesNoDialog(messagesBundle.getString("DialogImportDataTitle"), messagesBundle.getString("DialogImportDataRooms"))) {
                sourcefile = showFileChooser(messagesBundle.getString("FileChooserTitle"), true);
                if (sourcefile != null) { importRooms(sourcefile); }
            }
        }
    }

    //import
    private void importConfig () {
        File file = new File("config_server.xml");

        if(file.exists() && !file.isDirectory()) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(server.Config.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                config = (server.Config) jaxbUnmarshaller.unmarshal(file);
            } catch (JAXBException e) { addToLog(e.getMessage(), true); }
        } else {
            config = new server.Config();
            config.setPort(1789);
            config.setBackupPath(new File(System.getProperty("user.home")));

            ArrayList<String> defaultForbiddenWords = new ArrayList<>();
            defaultForbiddenWords.add("server");
            defaultForbiddenWords.add("admin");
            config.setForbiddenWords(defaultForbiddenWords);
        }

        Locale.setDefault( new Locale("en") );
        try {
            String baseName = "resources.Messages";
            messagesBundle = ResourceBundle.getBundle(baseName, new UTF8Control());
        } catch ( MissingResourceException e ) { addToLog(e.getMessage(),true); }
    }

    private void importUsers(File source) {
        try {
            JAXBContext context = JAXBContext.newInstance(ChatUserWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            ChatUserWrapper wrapper = (ChatUserWrapper) um.unmarshal(source);
            List<ChatUser> list = wrapper.getUsers();

            for (int i = 0;i<list.size(); i++) {
                ChatUser current = list.get(i);
                current.setId(i);
            } server.setUsersList(list);
        } catch (Exception e) {
            addToLog(e.getMessage(), true);
            showErrorDialog("ErrorImportUsersTitle", "ErrorImportUsersContent");
        }
    }

    private void importRooms(File source) {
        try {
            JAXBContext context = JAXBContext.newInstance(ChatRoomWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            ChatRoomWrapper wrapper = (ChatRoomWrapper) um.unmarshal(source);
            List<ChatRoom> list = wrapper.getRooms();

            for (int i = 0;i<list.size(); i++) {
                ChatRoom current = list.get(i);
                current.setId(i);
            } server.setRoomsList(wrapper.getRooms());
        } catch (Exception e) {
            addToLog(e.getMessage(), true);
            showErrorDialog("ErrorImportRoomsTitle", "ErrorImportRoomsContent");
        }
    }

    //finish
    @Override
    public void stop() throws Exception {
        super.stop();
        if (!error) {
            exportConfig();
            if (showYesNoDialog(messagesBundle.getString("DialogStopServerTitle"), messagesBundle.getString("DialogStopServerContent"))) {
                if (config.getBackupPath() != null) {
                    saveUsers(config.getBackupPath());
                    saveRooms(config.getBackupPath());
                }
            }
            if (logWriter != null) {logWriter.close();}
        }
    }

    //export
    private void exportConfig () {
        try {
            File file = new File("config_server.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(server.Config.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(config, file);
        } catch (JAXBException e) { addToLog(e.getMessage(), true); }
    }

    private void saveUsers(File savefolder) {
        try {
            JAXBContext context = JAXBContext.newInstance(ChatUserWrapper.class);
            Marshaller jaxbMarshaller = context.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            ChatUserWrapper wrapper = new ChatUserWrapper();
            wrapper.setUsers(getServer().getUsersArrayList());

            jaxbMarshaller.marshal(wrapper, new File( savefolder, "backup_users.xml"));

        } catch (Exception e) {
            addToLog(e.getMessage(), true);
            showErrorDialog("ErrorSaveUsersTitle", "ErrorSaveUsersContent");
        }
    }

    private void saveRooms(File savefolder) {
        try {
            JAXBContext context = JAXBContext.newInstance(ChatRoomWrapper.class);
            Marshaller jaxbMarshaller = context.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            ChatRoomWrapper wrapper = new ChatRoomWrapper();
            wrapper.setRooms(getServer().getRoomsArrayList());

            jaxbMarshaller.marshal(wrapper, new File( savefolder, "backup_rooms.xml"));

        } catch (Exception e) {
            addToLog(e.getMessage(), true);
            showErrorDialog("ErrorSaveRoomsTitle", "ErrorSaveRoomsContent");
        }
    }


    /* ------------------------------------- dialogs -------------------------------- */
    String showInputTextDialog(String title, String content, String defaultValue) {
        TextInputDialog dialog = new TextInputDialog(defaultValue);
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.setContentText(content);
        dialog.initStyle(StageStyle.UTILITY);
        loadStyle(dialog.getDialogPane());
        Optional<String> result = dialog.showAndWait();

        return result.orElse(defaultValue);
    }

    boolean showYesNoDialog (String title, String content) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.initStyle(StageStyle.UTILITY);
        ButtonType buttonTypeYes = new ButtonType(messagesBundle.getString("Yes"));
        ButtonType buttonTypeNo = new ButtonType(messagesBundle.getString("No"));
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
        loadStyle(alert.getDialogPane());

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == buttonTypeYes;
    }


    private File showFileChooser(String title, boolean filterXML) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.setInitialDirectory(config.getBackupPath());

        if (filterXML) { fileChooser.getExtensionFilters().addAll(
                    /*new FileChooser.ExtensionFilter("All Files", "*.*"), //*/
                    new FileChooser.ExtensionFilter("XML", "*.xml"));
        }

        return fileChooser.showOpenDialog(dashboardStage);
    }

    File showDirectoryChooser(String title) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(title);
        directoryChooser.setInitialDirectory(config.getBackupPath());
        return directoryChooser.showDialog(dashboardStage);
    }

    private void showErrorDialog(String title, String content) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(messagesBundle.getString(title));
        alert.setHeaderText(null);
        alert.setContentText(messagesBundle.getString(content));
        error = true;
        loadStyle(alert.getDialogPane());
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }

    String showSelectUserDialog (ArrayList<ChatUser> userArrayList) {
        if (!userArrayList.isEmpty()) {
            List<String> choices = new ArrayList<>();
            for (ChatUser anUserList : userArrayList) { choices.add(anUserList.getUsername()); }
            ChoiceDialog<String> dialog = new ChoiceDialog<>(messagesBundle.getString("DialogSelectUserDefault"), choices);
            dialog.setTitle(messagesBundle.getString("DialogSelectUserTitle"));
            dialog.setHeaderText(null);
            dialog.setContentText(messagesBundle.getString("DialogSelectUserContent"));
            dialog.initStyle(StageStyle.UTILITY);
            loadStyle(dialog.getDialogPane());
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){ return result.get(); }
        } return null;
    }


    /* ------------------------------------- setter -------------------------------- */



    /* ------------------------------------- getter -------------------------------- */
    ChatServer getServer() {return server;}
    DashboardController getDashboardController() {return dashboardController;}
    server.Config getConfig() { return config; }
    public ResourceBundle getMessagesBundle() { return messagesBundle; }


    /* ------------------------------------- other -------------------------------- */
    void addToLog (String content, boolean error) {
        try {
            if (error) {logWriter.append("\n\n--------------------------- Error --------------------------------------------------\n");}
            logWriter.append(content).append("\n");
            if (error) {logWriter.append("------------------------------------------------------------------------------------\n");}
            logWriter.flush();
        } catch (IOException e) { e.printStackTrace(); }
    }

    private void loadStyle (DialogPane dialogPane) {
        dialogPane.getStylesheets().add(getClass().getResource("../resources/theme.css").toExternalForm());
        dialogPane.getStylesheets().add(getClass().getResource(skin.getCSSPath()).toExternalForm());
        dialogPane.getStyleClass().add("gradientbackground");
    }
}
