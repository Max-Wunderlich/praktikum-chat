package server;

import items.ItemChatMessageLog;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import models.ChatMessage;
import models.ChatRoom;
import models.ChatUser;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

public class DashboardController {
    //General
    private Main mainApp;

    //Log
    @FXML
    ListView<ChatMessage> lvLog;
    private ObservableList<ChatMessage> listLog = FXCollections.observableArrayList();

    //User
    @FXML
    ListView<String> lvUsers;
    private ObservableList<String> listUsernames = FXCollections.observableArrayList();
    private ChangeListener<String> lvUsersListener;
    @FXML Label labelUserUsername, labelUserRoom, labelUserIp;

    //Rooms
    @FXML
    ListView<String> lvRooms;
    private ObservableList<String> listRooms = FXCollections.observableArrayList();
    private ChangeListener<String> lvRoomsListener;
    @FXML Label labelRoomRoomname, labelRoomId, labelRoomNumberOfUsers;

    //Connection
    @FXML
    HBox hbProgressConnection;
    @FXML
    Label labelProgressConnection;


    /* ------------------------------------- procedure methods -------------------------------- */
    @FXML
    private void initialize() {
        lvLog.setItems(listLog);
        lvUsers.setItems(listUsernames);
        lvRooms.setItems(listRooms);

        lvLog.setCellFactory(new Callback<ListView<ChatMessage>, ListCell<ChatMessage>>() {
            @Override
            public ListCell<ChatMessage> call(ListView<ChatMessage> list) {
                return new ItemChatMessageLog(mainApp);
            }
        });

        lvUsersListener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.isEmpty()) {
                    ChatUser tempUser = mainApp.getServer().getUserObjectFromList(newValue);
                    ChatServer.ConnectionThread tempThread = mainApp.getServer().getUserConnection(newValue);

                    labelUserUsername.setText(tempUser.getUsername());
                    labelUserRoom.setText(mainApp.getServer().getRoomObjectFromList(tempThread.getRoom()).getRoomname());
                    labelUserIp.setText(tempThread.getIP());
                }
            }
        };

        lvUsers.getSelectionModel().selectedItemProperty().addListener(lvUsersListener);

        lvRoomsListener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.isEmpty()) {
                    ChatRoom tempRoom = mainApp.getServer().getRoomObjectFromList(newValue);
                    labelRoomRoomname.setText(tempRoom.getRoomname());
                    labelRoomId.setText(String.valueOf(tempRoom.getID()));
                    labelRoomNumberOfUsers.setText(String.valueOf(mainApp.getServer().getNumberOfUsersInRoom(tempRoom.getID())));
                }
            }
        };

        lvRooms.getSelectionModel().selectedItemProperty().addListener(lvRoomsListener);

        updateUserlist("");
    }

    @FXML
    public void shutdown () {
        if (mainApp.getServer().stopService()) {
            Platform.exit();
        } else {
            displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorServerStop",ChatMessage.ICONERROR));
        }
    }


    /* ------------------------------------- user actions -------------------------------- */
    @FXML
    private void warnUser() {
        String victim = mainApp.showSelectUserDialog(mainApp.getServer().getOnlineUsersArrayList());
        if (mainApp.getServer().findInUserList(victim)) {
            mainApp.getServer().getUserConnection(victim).sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "WarnUser", ChatMessage.ICONSERVER));
        } else { displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorFailKick", ChatMessage.ICONERROR)); }
    }

    @FXML
    private void kickUser () {
        String victim = mainApp.showSelectUserDialog(mainApp.getServer().getOnlineUsersArrayList());
        if (mainApp.getServer().findInUserList(victim) && mainApp.getServer().searchUser(victim)) {
            mainApp.getServer().kickOnlineUser(victim);
        } else { displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorFailBan", ChatMessage.ICONERROR)); }
    }

    @FXML
    private void banUser () {
        String victim = mainApp.showSelectUserDialog(mainApp.getServer().getUsersArrayList());
        if (mainApp.getServer().findInUserList(victim)) {
            ChatUser victimObject = mainApp.getServer().getUserObjectFromList(victim);
            victimObject.setBan(true);
            if (mainApp.getServer().searchUser(victim)) { mainApp.getServer().kickOnlineUser(victim); }
        } else { displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorFailKick", ChatMessage.ICONERROR)); }
    }


    /* ------------------------------------- room actions -------------------------------- */
    @FXML
    public void createRoom () {
        String tempname = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogCreateRoomTitle"), mainApp.getMessagesBundle().getString("DialogCreateRoomContentName"), "");
        if (!tempname.isEmpty() && !tempname.contains("*") && !mainApp.getServer().compareRoomname(tempname) && mainApp.getConfig().checkContent(tempname)) {
            if (mainApp.showYesNoDialog(mainApp.getMessagesBundle().getString("DialogCreateRoomTitle"), mainApp.getMessagesBundle().getString("DialogCreateRoomContentPrivate"))) {
                String temppassword = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogCreateRoomTitle"), mainApp.getMessagesBundle().getString("DialogCreateRoomContentPassword"), "");
                mainApp.getServer().createRoom(tempname, temppassword);
            } else { mainApp.getServer().createRoom(tempname, ""); }
        } else { displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorCreateRoom", ChatMessage.ICONERROR)); }
    }

    @FXML
    public void editRoomName () {
        int input = lvRooms.getSelectionModel().getSelectedIndex();
        if (input > 0) { mainApp.getServer().editRoomName(listRooms.get(input)); }
    }

    @FXML
    public void editRoomPassword () {
        int input = lvRooms.getSelectionModel().getSelectedIndex();
        if (input > 0) { mainApp.getServer().editRoomPassword(listRooms.get(input)); }
    }

    @FXML
    public void deleteRoom() {
        int input = lvRooms.getSelectionModel().getSelectedIndex();
        if (input > 0) { mainApp.getServer().deleteRoom(listRooms.get(input)); }
    }


    /* ------------------------------------- update view -------------------------------- */
    public void displayMessage (ChatMessage message) {
        int index = lvLog.getItems().size();

        Platform.runLater(() -> {
            listLog.add(message);
            lvLog.scrollTo(index);
        });
        mainApp.addToLog(message.getContent() + " - " + message.getMetaAsString(), false);
    }

    void updateUserlist (String list) {
        Platform.runLater(() -> {
            lvUsers.getSelectionModel().selectedItemProperty().removeListener(lvUsersListener);
            lvUsers.setItems(null);
            listUsernames.clear();
            String[] parts = list.split("‰");
            listUsernames.addAll(Arrays.asList(parts));
            lvUsers.setItems(listUsernames);
            lvUsers.getSelectionModel().selectedItemProperty().addListener(lvUsersListener);
        });
    }

    void updateRoomlist (String list) {
        Platform.runLater(() -> {
            lvRooms.getSelectionModel().selectedItemProperty().removeListener(lvRoomsListener);
            lvRooms.setItems(null);
            listRooms.clear();
            String[] parts = list.split("‰");
            listRooms.addAll(Arrays.asList(parts));
            lvRooms.setItems(listRooms);
            lvRooms.getSelectionModel().selectedItemProperty().addListener(lvRoomsListener);
        });
    }

    void showProgressbar () { hbProgressConnection.setVisible(true); }
    void updateProgressbar () { Platform.runLater(() -> { labelProgressConnection.setText(mainApp.getMessagesBundle().getString("WaitingNewConnection") + "  -  " + mainApp.getMessagesBundle().getString("NumberOfConnections") + ": " + mainApp.getServer().getNumberOfConnections()); }); }


    /* ------------------------------------- other actions -------------------------------- */
    @FXML
    public void sendServerMessageToAllClients(){
        String servermessage = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogServerMessageTitle"), mainApp.getMessagesBundle().getString("DialogServerMessageContent"), "");
        if (!servermessage.isEmpty()) { mainApp.getServer().sendMessageToAll(new ChatMessage(ChatMessage.MESSAGE, servermessage, ChatMessage.ICONSERVER)); }
        displayMessage(new ChatMessage(ChatMessage.MESSAGE, servermessage, ChatMessage.ICONSERVER));
    }

    @FXML
    private void setBackupPath () {
        mainApp.getConfig().setBackupPath(mainApp.showDirectoryChooser(mainApp.getMessagesBundle().getString("FolderChooserTitle")));
        displayMessage(new ChatMessage(ChatMessage.MESSAGE, "Der Backuppfad wurde zu " + mainApp.getConfig().getBackupPath().toString() + "geändert!",ChatMessage.ICONALERT));//??????
    }

    @FXML
    private void addForbiddenWord () {
        String input = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogAddForbiddenWordTitle"), mainApp.getMessagesBundle().getString("DialogAddForbiddenWordContent"), "");
        if (!input.isEmpty() && mainApp.getConfig().checkContent(input)) { mainApp.getConfig().addForbiddenWord(input); }
    }

    @FXML
    private void displayIp () {
        StringBuilder message = new StringBuilder();
        Enumeration e;
        try {
            e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements()){    NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements())    {
                    InetAddress i = (InetAddress) ee.nextElement();
                    if (!i.getHostAddress().contains(":") && !i.getHostAddress().isEmpty() && !i.getHostAddress().contains("127.0.0.1")) { message.append(System.getProperty("line.separator")).append(i.getHostAddress()); }
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("ServerGetIP") + message.toString(), ChatMessage.ICONALERT));
    }

    @FXML
    void openHelpPage () throws URISyntaxException {
        try { openWebpage( new URL("http://uni.maxwunderlich.de/informatik/chat/help.php").toURI()); }
        catch (MalformedURLException e) { mainApp.addToLog(e.getMessage(), true); }
    }

    @FXML
    void openAboutPage () throws URISyntaxException {
        try { openWebpage( new URL("http://uni.maxwunderlich.de/informatik/chat/about.php").toURI()); }
        catch (MalformedURLException e) { mainApp.addToLog(e.getMessage(), true); }
    }


    /* ------------------------------------- getter -------------------------------- */
    public ArrayList<ChatMessage> getLogContent () { return new ArrayList<>(listLog); }


    /* ------------------------------------- setter -------------------------------- */
    public void setMainApp (Main mainApp) { this.mainApp = mainApp; }


    /* ------------------------------------- other -------------------------------- */
    private static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try { desktop.browse(uri); }
            catch (Exception e) { e.printStackTrace(); }
        }
    }
}
