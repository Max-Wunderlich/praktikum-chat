package server;

import models.ChatMessage;
import models.ChatRoom;
import models.ChatUser;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    //General
    private Main mainApp;

    //Connection
    private ServerSocket serverSocket;
    private Socket client;
    private int portPrivate = 1871;

    //ChatUser
    private ArrayList<ChatUser> usersList;
    private int userid;

    //Thread
    private ArrayList<ConnectionThread> connectionList;

    //ChatRoom
    private ArrayList<ChatRoom> roomsList;
    private int roomid;

    private int port;


    ChatServer(int port, Main mainApp) {
        this.port = port;
        this.mainApp = mainApp;

        connectionList = new ArrayList<ConnectionThread>();
        usersList = new ArrayList<ChatUser>();
        roomsList = new ArrayList<ChatRoom>();

        roomsList.add(new ChatRoom("default", roomid++));
        refreshRoomList();
    }


    /* ------------------------------------- procedure methods -------------------------------- */
    public boolean initializeService (){
        userid = usersList.size();
        roomid = roomsList.size();
        System.out.println(roomid);
        System.out.println(String.valueOf(portPrivate++));

        try {
            serverSocket = new ServerSocket(port);
            LoginThread loginThread = new LoginThread(serverSocket, mainApp);
            loginThread.start();
            return true;
        } catch (IOException e) {
            mainApp.addToLog(e.getMessage(), true);
            return false;
        }
    }

    private void removeConnection (ConnectionThread thread) {
        connectionList.remove(thread);
        mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "SuccessConnectionRemove"));
        mainApp.getDashboardController().updateProgressbar();
    }

    boolean stopService() {
        if (connectionList.size() == 0) {
            try {
                serverSocket.close();
                System.out.println("--- Socket closed ---");
                return true;
            } catch (IOException e) { mainApp.addToLog(e.getMessage(), true); }
        }
        return false;
    }


    /* ------------------------------------- user action -------------------------------- */
    private boolean changePassword (String username, String password) {
        ChatUser user = getUserObjectFromList(username);
        if (user != null) {
            user.setPassword(password);
            return true;
        } return false;
    }

    void kickOnlineUser (String username) {
        for (ConnectionThread current : connectionList) {
            if (current.getLoginStatus()){
                if (current.getUsername().equals(username)) { current.sendMessageToClient(new ChatMessage(ChatMessage.KICK)); }
            }
        }
    }

    boolean searchUser(String username) {
        if (!connectionList.isEmpty()) {
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) {
                    if (current.getUsername().equals(username)) {
                        mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, username));
                        return true;
                    }
                }
            }
        } return false;
    }

    boolean findInUserList (String username) {
        if (!usersList.isEmpty()) {
            for (ChatUser anUserList : usersList) {
                String tempusername = anUserList.getUsername();
                if (tempusername.equals(username)) { return true; }
            }
        } return false;
    }


    /* ------------------------------------- room action -------------------------------- */
    public void createRoom(String roomname, String password) {
        roomsList.add(new ChatRoom(roomname, password, roomid++));
        mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "SuccessCreateRoom"));
        refreshRoomList();
    }

    public void editRoomName (String name) {
        ChatRoom targetRoom = getRoomObjectFromList(name);
        if (targetRoom != null) {
            String input = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogEditRoomnameTitle"), mainApp.getMessagesBundle().getString("DialogEditRoomnameTitle"), targetRoom.getRoomname()).trim();
            if (!input.isEmpty() && !input.equals(targetRoom.getRoomname())) {
                targetRoom.setName(input);
                mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "SuccessEditRoom", ChatMessage.ICONINFO));
                refreshRoomList();
                ChatMessage clientmessage = new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("AlertRoomnameChange") + " " + targetRoom.getRoomname(), "AlertRoomnameChange" + "½" + targetRoom.getRoomname(), ChatMessage.ICONALERT);
                clientmessage.addInformation("room", targetRoom.getRoomname());
                sendMessageToRoom(clientmessage, targetRoom.getID());
            } else { mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorEditRoomInput", ChatMessage.ICONERROR)); }
        } else { mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorEditRoomNotFound", ChatMessage.ICONERROR)); }
    }

    public void editRoomPassword (String name) {
        ChatRoom targetRoom = getRoomObjectFromList(name);
        if (targetRoom != null) {
            String input = mainApp.showInputTextDialog(mainApp.getMessagesBundle().getString("DialogEditRoomPasswordTitle"), mainApp.getMessagesBundle().getString("DialogEditRoomPasswordContent"), "").trim();
            targetRoom.setPassword(input);
            mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "SuccessEditRoom", ChatMessage.ICONINFO));
            refreshRoomList();
        } else { mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorEditRoomNotFound", ChatMessage.ICONERROR)); }
    }

    public void deleteRoom (String roomname) {
        ChatRoom targetRoom = getRoomObjectFromList(roomname);
        if (targetRoom != null && targetRoom.getID() > 0) {
            if (mainApp.showYesNoDialog(mainApp.getMessagesBundle().getString("DialogDeleteRoomnameTitle"), mainApp.getMessagesBundle().getString("DialogDeleteRoomnameContent"))) {
                for (ConnectionThread current : connectionList) {
                    if (current.getUsername()!=null) {
                        if (current.getRoom() == targetRoom.getID()) {
                            current.setRoom(0);
                            current.sendMessageToClient(new ChatMessage(ChatMessage.CHANGEROOM, "default"));
                            current.sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList()));
                            current.sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "AlertChangeRoom", ChatMessage.ICONALERT));
                        }
                    }
                }
                roomsList.remove(targetRoom);
                mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "SuccessDeletingRoom"));
                refreshRoomList();
            }
        } else { mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ErrorDeletingRoom")); }
    }

    public boolean compareRoomname(String roomname) {
        if (!roomsList.isEmpty()) {
            for (ChatRoom anRoomList : roomsList) {
                String tempRoomname = anRoomList.getRoomname();
                if (tempRoomname.equals(roomname)) { return true; }
            }
        } return false;
    }


    /* ------------------------------------- send methods -------------------------------- */
    void sendMessageToAll(ChatMessage message) {
        if (!connectionList.isEmpty()){
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) { current.sendMessageToClient(message); }
            }
        }
    }

    private void sendMessageToRoom(ChatMessage message, int roomid) {
        if (!connectionList.isEmpty()){
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) {
                    if (current.getRoom() == roomid) { current.sendMessageToClient(message); }
                }
            }
        }
    }


    /* ------------------------------------- refresh data -------------------------------- */
    private void refreshUserlist() {
        if (!connectionList.isEmpty()){
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) { current.sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList(current.getRoom())));}
            }
            mainApp.getDashboardController().updateUserlist(getUsersList());
        }
    }

    private void refreshRoomList() {
        if (!connectionList.isEmpty()){
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) { current.sendMessageToClient(new ChatMessage(ChatMessage.ROOMLIST, getRoomList())); }
            }
        }
        mainApp.getDashboardController().updateRoomlist(getRoomList());
    }


    /* ------------------------------------- getter -------------------------------- */
    //User
    private String getUsersList() {
        StringBuilder output = new StringBuilder();
        for (ConnectionThread current : connectionList) {
            if (current.getUsername()!=null) { output.append(current.getUsername()).append("‰"); }
        } return output.toString();
    }

    private String getUsersList(int room) {
        StringBuilder output = new StringBuilder();
        for (ConnectionThread current : connectionList) {
            if (current.getUsername()!=null) {
                if (current.getRoom() == room) { output.append(current.getUsername()).append("‰"); }
            }
        } return output.toString();
    }

    private String getOnlineUserMetaList() {
        StringBuilder output = new StringBuilder();
        for (ConnectionThread current : connectionList) {
            if (current.getUsername()!=null) { output.append(current.getUsername()).append("¾").append(current.getId()).append("‰"); }
        } return output.toString();
    }

    ArrayList<ChatUser> getOnlineUsersArrayList () {
        ArrayList<ChatUser> output = new ArrayList<ChatUser>();

        if (!connectionList.isEmpty()){
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) { output.add(getUserObjectFromList(current.getUsername())); }
            }
            mainApp.getDashboardController().updateUserlist(getUsersList());
        }
        return output;
    }

    //Connections
    int getNumberOfConnections () { return connectionList.size(); }

    ConnectionThread getUserConnection(String username) {
        if (!connectionList.isEmpty()) {
            for (ConnectionThread current : connectionList) {
                if (current.getLoginStatus()) {
                    if (current.getUsername().equals(username)) { return current; }
                }
            }
        } return null;
    }

    //Room
    private String getRoomList() {
        StringBuilder output = new StringBuilder();
        for (ChatRoom current : roomsList) {
            if (current.isPrivate()) {
                output.append("*");
            }
            output.append(current.getRoomname()).append("‰");
        }
        return output.toString();
    }

    private String getRoomMetaList() {
        StringBuilder output = new StringBuilder();
        for (ChatRoom current : roomsList) {
            if (current.isPrivate()) {
                output.append("¼");
            }
            output.append(current.getRoomname()).append("¾").append(current.getID()).append("‰");
        }
        return output.toString();
    }

    int getNumberOfUsersInRoom (int roomid) {
        int count = 0;
        if (roomid > -1) {
            if (!connectionList.isEmpty()){
                for (ConnectionThread current : connectionList) {
                    if (current.getLoginStatus()) {
                        if (current.getRoom() == roomid) { count++; }
                    }
                }
            }
        }
        return count;
    }

    public ArrayList<ChatUser> getUsersArrayList () { return usersList; }
    public ArrayList<ChatRoom> getRoomsArrayList () { return roomsList; }

    ChatUser getUserObjectFromList(String username) {
        for (ChatUser anUserList : usersList) {
            if (anUserList.getUsername().equals(username)) { return anUserList; }
        } return null;
    }

    ChatRoom getRoomObjectFromList(String roomname) {
        String temp = roomname;
        if (temp.substring(0,1).equals("*")) {
            temp = temp.substring(1);
        }
        for (ChatRoom element : roomsList) {
            if (element.getRoomname().equals(temp)) { return element; }
        } return null;
    }

    ChatRoom getRoomObjectFromList(int id) {
        for (ChatRoom element : roomsList) {
            if (element.getID() == id) { return element; }
        } return null;
    }


    /* ------------------------------------- setter -------------------------------- */
    void setUsersList (List<ChatUser> source) {
        usersList.clear();
        usersList.addAll(source);
        userid = source.size();
        mainApp.getDashboardController().updateUserlist(getUsersList());
    }

    void setRoomsList (List<ChatRoom> source) {
        roomsList.clear();
        roomsList.addAll(source);
        roomid = source.size();
        mainApp.getDashboardController().updateRoomlist(getRoomList());
    }


    /* ------------------------------------------------------------------------------ */
    /* ------------------------------------- THREADS -------------------------------- */
    /* ------------------------------------------------------------------------------ */
    class ConnectionThread extends Thread {
        //Gerneral
        Main mainApp;
        int id;

        //Connection
        Socket socket;
        ObjectInputStream inputStream;
        ObjectOutputStream outputStream;
        String ip;

        //UserInfo
        String username;
        int room;


        ConnectionThread(Socket socket, Main mainApp) {
            this.socket = socket;
            this.mainApp = mainApp;
            username = "";
            if (socket.getRemoteSocketAddress().toString().contains("127.0.0.1")) {
                ip = "10.232.51.203";
            } else {
                ip = socket.getRemoteSocketAddress().toString();
                ip = ip.substring(1); //  / raus
                ip = ip.split(":")[0];
                System.out.println(ip);
            }
        }


        @Override
        public void run() {
            try {
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                inputStream = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                mainApp.addToLog(e.getMessage(), true);
                removeConnection(this);
                return;
            }

            Boolean online = true;

            while (online) {
                try {
                    ChatMessage message = (ChatMessage) inputStream.readObject();
                    message.generateTimestamp();
                    message.addInformation("ip", ip);
                    switch (message.getType()) {
                        case ChatMessage.LOGIN: {
                            String tempUsername = message.getContent();
                            String tempPassword = message.getInformation("password");
                            mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("Username")+":\t" + tempUsername));
                            String status = verifyUser(tempUsername, tempPassword);
                            ChatMessage tempMessage = new ChatMessage(ChatMessage.LOGIN, status);
                            tempMessage.addInformation("username", tempUsername);
                            outputStream.writeObject(tempMessage);

                            switch (status) {
                                case "register": {
                                    sendMessageToAll(new ChatMessage(ChatMessage.USERSTATUS, tempUsername + " is online", tempUsername + "½PartOnlineUser", ChatMessage.ICONSERVER));
                                    username = tempUsername;
                                    room = 0;
                                    id = getUserObjectFromList(username).getId();
                                    refreshUserlist();
                                    sendMessageToClient(new ChatMessage(ChatMessage.ROOMLIST, getRoomList()));
                                    sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "GreetingRegister", ChatMessage.ICONINFO));
                                    break;
                                }

                                case "login": {
                                    sendMessageToAll(new ChatMessage(ChatMessage.USERSTATUS, tempUsername + " is online", tempUsername + "½PartOnlineUser", ChatMessage.ICONSERVER));
                                    username = tempUsername;
                                    room = 0;
                                    id = getUserObjectFromList(username).getId();
                                    refreshUserlist();
                                    sendMessageToClient(new ChatMessage(ChatMessage.ROOMLIST, getRoomList()));
                                    sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "GreetingLogin", ChatMessage.ICONINFO));
                                    break;
                                }

                                case "fail-ban": {
                                    //Fail
                                    mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("PartErrorLoginBan")+ " [" + client.getRemoteSocketAddress() + "]"));
                                    online = false;
                                    break;
                                }
                                default: {
                                    mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, mainApp.getMessagesBundle().getString("PartErrorLogin")+ " [" + client.getRemoteSocketAddress() + "]"));
                                    break;
                                }
                            }
                            break;
                        }

                        case ChatMessage.LOGOUT: {
                            String tempUsername = username;
                            username = "";
                            if (!tempUsername.isEmpty()) { sendMessageToAll(new ChatMessage(ChatMessage.USERSTATUS, tempUsername + " is offline", tempUsername + "½PartOfflineUser", ChatMessage.ICONSERVER));}
                            if (message.findItemInMeta("close") > -1) { online = false; } //if message meta has close -> connection will shutdown
                            if (!tempUsername.isEmpty()) { mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE,tempUsername + mainApp.getMessagesBundle().getString("PartUserOffline")));}
                            break;
                        }

                        case ChatMessage.UPDATEPASSWORD: {
                            if (changePassword(message.getUsername(), message.getContent())) {
                                sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "SuccessEditPassword", ChatMessage.ICONSERVER));
                            } else { sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "ErrorEditPassword", ChatMessage.ICONERROR)); }
                            break;
                        }

                        case ChatMessage.CHANGEROOM: {
                            ChatRoom object = getRoomObjectFromList(message.getContent());
                            if (object != null){
                                if (!object.isPrivate()) {
                                    room = getRoomObjectFromList(message.getContent()).getID();
                                    sendMessageToClient(message);
                                    if (room > 0) {
                                        sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList(room)));
                                    } else {
                                        sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList()));
                                    }
                                    mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE,username + mainApp.getMessagesBundle().getString("PartEnterRoom") + room));
                                } else {
                                    //Password
                                    if (object.checkPassword(message.getInformation("password"))) {
                                        room = getRoomObjectFromList(message.getContent()).getID();
                                        sendMessageToClient(message);
                                        if (room > 0) {
                                            sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList(room)));
                                        } else {
                                            sendMessageToClient(new ChatMessage(ChatMessage.USERLIST, getUsersList()));
                                        }
                                        mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE,username + mainApp.getMessagesBundle().getString("PartEnterRoom") + room));
                                    } else {
                                        sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "ErrorChangeRoomPassword", ChatMessage.ICONERROR));
                                    }
                                }
                            } else {
                                sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "ErrorChangeRoom", ChatMessage.ICONERROR));
                            }
                            break;
                        }

                        case ChatMessage.CREATEROOM: {
                                if (!compareRoomname(message.getContent()) && mainApp.getConfig().checkContent(message.getContent())) {
                                    createRoom(message.getContent(), "");
                                    sendMessageToClient(new ChatMessage(ChatMessage.CREATEROOM, message.getContent()));
                                } else {
                                    sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "ErrorCreateRoom", ChatMessage.ICONERROR));
                                }
                            break;
                        }

                        case ChatMessage.CHECKNAME: {
                            if (mainApp.getConfig().checkContent(message.getContent())) {
                                //Zugelassen
                                sendMessageToClient(message);
                            } else {
                                sendMessageToClient(new ChatMessage(ChatMessage.CHECKNAME));
                            }
                            break;
                        }

                        case ChatMessage.PRIVATEMESSAGE: {
                            ChatMessage temp = new ChatMessage(ChatMessage.PRIVATEMESSAGE, String.valueOf(portPrivate++));
                            System.out.println(temp.getContent());
                            sendMessageToClient(temp);
                            temp.addInformation("ip", ip);
                            ConnectionThread partner = getUserConnection(message.getContent());
                            partner.sendMessageToClient(temp);
                            break;
                        }

                        case ChatMessage.MESSAGE: {
                            message.setRoom(getRoomObjectFromList(room).getRoomname());
                            if (room > 0) {
                                sendMessageToRoom(message, getRoom());
                            } else {
                                sendMessageToClient(message);
                                sendMessageToClient(new ChatMessage(ChatMessage.MESSAGE, "AlertNoWriteRight", ChatMessage.ICONALERT));
                            }
                            mainApp.getDashboardController().displayMessage(message);
                            break;
                        }

                        default: { mainApp.getDashboardController().displayMessage(message); }
                    }

                } catch (IOException | ClassNotFoundException e) {
                    online = false;
                }
            }
            removeConnection(this);
            refreshUserlist();
        }


        /* ------------------------------------- verify user -------------------------------- */
        private String verifyUser(String tempusername, String temppassword) {
            if (mainApp.getConfig().checkContent(tempusername)) {
                if (findInUserList(tempusername)) {
                    if (!searchUser(tempusername)) {
                        //Check Password
                        if (getUserObjectFromList(tempusername).checkPassword(temppassword)) {
                            if (!getUserObjectFromList(tempusername).isBan()) {
                                return "login";
                            } else { return "fail-ban"; }
                        } else { return "fail-input"; }
                    } else { return "fail-otherconnection"; }
                } else {
                    mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, tempusername + mainApp.getMessagesBundle().getString("PartCreateUser")));
                    usersList.add(new ChatUser(tempusername, temppassword, userid++));
                    return "register";
                }
            } else {return "fail-forbidden";}
        }


        /* ------------------------------------- send method -------------------------------- */
        public void sendMessageToClient(ChatMessage message) {
            try { outputStream.writeObject(message); }
            catch (IOException e) { mainApp.addToLog(e.getMessage(), true); }
        }


        /* ------------------------------------- getter -------------------------------- */
        public String getUsername() { return username; }
        boolean getLoginStatus() { return !username.isEmpty(); }
        public int getRoom() {return room;}
        public String getIP() { return ip; }


        /* ------------------------------------- setter -------------------------------- */
        public void setRoom(int room) { this.room = room; }
    }


    class LoginThread extends Thread {
        ServerSocket serverSocket;
        Main mainApp;
        boolean run;


        LoginThread(ServerSocket serverSocket, Main mainApp) {
            this.serverSocket = serverSocket;
            this.mainApp = mainApp;
            run = true;
        }


        @Override
        public void run() {
            mainApp.getDashboardController().showProgressbar();
            mainApp.getDashboardController().displayMessage(new ChatMessage(ChatMessage.MESSAGE, "ServerStart" ,ChatMessage.ICONALERT));

            while (run) {
                try {
                    client = serverSocket.accept();
                    ConnectionThread connectionThread = new ConnectionThread(client, mainApp);
                    connectionList.add(connectionThread);
                    connectionThread.start();
                    mainApp.getDashboardController().updateProgressbar();
                } catch (IOException e) { run = false; }
            } System.out.println("Thread closed");
        }
    }
}