package server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.ArrayList;

@XmlRootElement
public class Config {
    //Connection
    private int port;
    private File backupPath;
    private ArrayList<String> forbiddenWords;


    /* ------------------------------------- other -------------------------------- */
    public boolean checkContent (String content) {
        for (String current: forbiddenWords) {
            if (content.contains(current)) { return false; }
        } return true;
    }


    /* ------------------------------------- setter -------------------------------- */
    @XmlElement
    public void setPort(int port) { this.port = port; }
    @XmlElement
    public void setBackupPath(File backupPath) { this.backupPath = backupPath; }
    @XmlElement
    public void setForbiddenWords(ArrayList<String> forbiddenWords) { this.forbiddenWords = forbiddenWords; }
    public void addForbiddenWord (String word) { forbiddenWords.add(word.toLowerCase()); }


    /* ------------------------------------- getter -------------------------------- */
    public int getPort() { return port; }
    public File getBackupPath() { return backupPath; }
    public ArrayList<String> getForbiddenWords() { return forbiddenWords; }
}